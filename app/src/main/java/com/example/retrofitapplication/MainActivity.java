package com.example.retrofitapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class MainActivity extends AppCompatActivity {

    private TextView textViewJSON ; // TextView dans lequel on va insérer le JSON récupéré de l'API

    // URL de base de l'API (doit se terminer par /)
    private static final String API_BASE_URL = "https://pokeapi.co/api/v2/";

    // Instance nécessaires au traitement (pour Retrofit)
    Retrofit retrofit;
    PokemonAPIService servicePokemon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // récupération du textView
        this.textViewJSON = (TextView) findViewById(R.id.idTextView);

        // Construction d'une instance de retrofit (Etape #2 du cours)
        this.retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.servicePokemon = retrofit.create(PokemonAPIService.class);


        // Construit le traitement lorsque l'on clique sur le bouton
        Button bouton = (Button) findViewById(R.id.idButton);

        bouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Fait un appel à la méthode listPokemons
                // >>> https://pokeapi.co/api/v2/pokemon?limit=20&offset=200 (cf resultat en bas de ce fichier)
                Call<JsonElement> appel = servicePokemon.listPokemons();

                // Appel asynchrone (a privilégier)
                appel.enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        if (response.isSuccessful()){
                            // Récupère le contenu de la réponse
                            JsonElement contenu = response.body();

                            // 1er niveau de traitement : on affiche le contenu "brut"
                            //textViewJSON.setText(contenu.toString());

                            // #################################################

                            //2ème niveau on extrait et affiche chaque pokemon de la liste
                            //Convertir en JsonObject (type de GSON et non JSONObject défini dans java)
                            JsonObject jsonGlobal = contenu.getAsJsonObject();

                            // On peut du coup parcourir les éléments
                            // Affichage du nombre de pokemons
                            int count = jsonGlobal.get("count").getAsInt();
                            Toast.makeText(MainActivity.this, "Nb Pokemons total : "+count, Toast.LENGTH_SHORT).show();

                            // l'attribut "results" est un tableau de documents
                            JsonArray listePokemons = jsonGlobal.getAsJsonArray("results");

                            // On peut alors utiliser un itérateur plutôt qu'une boucle indicée...
                            // On pourrait aussi utiliser += sur les chaînes mais moins efficace et memory friendly
                            StringBuffer chaineConstruite = new StringBuffer();
                            for (int i = 0 ; i < listePokemons.size(); i++){
                                // On récupère le document à la position i de la liste
                                JsonObject unPokemon = listePokemons.get(i).getAsJsonObject();
                                // Ajoute à la chaine
                                chaineConstruite.append(unPokemon.get("name").getAsString()+"\n");
                            }

                            // Affiche la chaine sur l'interface
                            textViewJSON.setText(chaineConstruite.toString());
                        }
                        else{
                            Toast.makeText(MainActivity.this, "Erreur lors de l'appel à l'API :"+response.errorBody(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Erreur lors de l'appel à l'API :" +t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}




// Etape #1 - Création de l'API (Endpoint)
// URL de base = https://pokeapi.co/api/v2/  (doit se terminer par /)
interface PokemonAPIService{
    // 1er appel possible
    @GET("pokemon?limit=20&offset=200")
    Call<JsonElement> listPokemons();

    // 2ème appel possible
    @GET("pokemon/{IDPOKEMON}")
    Call<JsonElement> getPokemon(@Path("IDPOKEMON") String id);
}




/*
 Contenu de l'URL appelée par listPokemons
{"count":1118,"next":"https://pokeapi.co/api/v2/pokemon?offset=220&limit=20","previous":"https://pokeapi.co/api/v2/pokemon?offset=180&limit=20",
"results":[
{"name":"unown","url":"https://pokeapi.co/api/v2/pokemon/201/"},{"name":"wobbuffet","url":"https://pokeapi.co/api/v2/pokemon/202/"},
{"name":"girafarig","url":"https://pokeapi.co/api/v2/pokemon/203/"},{"name":"pineco","url":"https://pokeapi.co/api/v2/pokemon/204/"},
{"name":"forretress","url":"https://pokeapi.co/api/v2/pokemon/205/"},{"name":"dunsparce","url":"https://pokeapi.co/api/v2/pokemon/206/"},
{"name":"gligar","url":"https://pokeapi.co/api/v2/pokemon/207/"},{"name":"steelix","url":"https://pokeapi.co/api/v2/pokemon/208/"},
{"name":"snubbull","url":"https://pokeapi.co/api/v2/pokemon/209/"},{"name":"granbull","url":"https://pokeapi.co/api/v2/pokemon/210/"},
{"name":"qwilfish","url":"https://pokeapi.co/api/v2/pokemon/211/"},{"name":"scizor","url":"https://pokeapi.co/api/v2/pokemon/212/"},
{"name":"shuckle","url":"https://pokeapi.co/api/v2/pokemon/213/"},{"name":"heracross","url":"https://pokeapi.co/api/v2/pokemon/214/"},
{"name":"sneasel","url":"https://pokeapi.co/api/v2/pokemon/215/"},{"name":"teddiursa","url":"https://pokeapi.co/api/v2/pokemon/216/"},
{"name":"ursaring","url":"https://pokeapi.co/api/v2/pokemon/217/"},
{"name":"slugma","url":"https://pokeapi.co/api/v2/pokemon/218/"},{"name":"magcargo","url":"https://pokeapi.co/api/v2/pokemon/219/"},
{"name":"swinub","url":"https://pokeapi.co/api/v2/pokemon/220/"}
]}

 */